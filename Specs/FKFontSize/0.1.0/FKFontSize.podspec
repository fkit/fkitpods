#
# Be sure to run `pod lib lint FKFontSize.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "FKFontSize"
  s.version          = "0.1.0"
  s.summary          = "A short description of FKFontSize."

  s.license          = 'FaithKIT'
  s.author           = { 'MakeMeBetter' => 'nezhelskoy.aleksey@nixsolutions.com' }
  s.source           = { :git => "https://nezhelskoy@bitbucket.org/fkit/fkitvendors.git" }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'FKFontSize' => ['Pod/Assets/*.png']
  }

  s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
end
